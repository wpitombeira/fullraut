<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");


include('config.php');
    $link = DBConnect();

    if (mysqli_real_escape_string($link,$_POST['TempInt']) ==NULL ||mysqli_real_escape_string($link,$_POST['TempInt']) ==NAN){
        $TempInt="NULL";
    }else{
        $TempInt=mysqli_real_escape_string($link,$_POST['TempInt']);
    }
    if (mysqli_real_escape_string($link,$_POST['TempExt']) ==NULL ||mysqli_real_escape_string($link,$_POST['TempExt']) ==NAN){
        $TempExt="NULL";
    }else{
        $TempExt=mysqli_real_escape_string($link,$_POST['TempExt']);
    }

    if (mysqli_real_escape_string($link,$_POST['HumidInt']) ==NULL){
        $HumidInt="NULL";
    }else{
        $HumidInt=mysqli_real_escape_string($link,$_POST['HumidInt']);
    }
    if (mysqli_real_escape_string($link,$_POST['HumidExt']) ==NULL){
        $HumidExt="NULL";
    }else{
        $HumidExt=mysqli_real_escape_string($link,$_POST['HumidExt']);
    }
    if(mysqli_real_escape_string($link,$_POST['gasLevel']) == NULL){
        $gasLevel = "NULL";
    } else {
        $gasLevel = mysqli_real_escape_string($link,$_POST['gasLevel']);
    }

    $logDate    = date("Y-m-d H:i:s");

    $sqlInternal = "INSERT INTO tb_TemperatureInternal (date, temperature, humidity) values('{$logDate}', '{$TempInt}', '{$HumidInt}')";
    $sqlExternal = "INSERT INTO tb_TemperatureExternal (date, temperature, humidity) values('{$logDate}', '{$TempExt}', '{$HumidExt}')";
    $sqlGas = "INSERT INTO tb_SensorGas (date, gasLevel) values('{$logDate}', '{$gasLevel}')";

    if(mysqli_query($link,$sqlInternal) && mysqli_query($link,$sqlExternal) && mysqli_query($link,$sqlGas)){
        return http_response_code(200);
    } else {
        return http_response_code(500);
    }

    $sqlInternal = null;
    $sqlExternal = null;
    $HumidExt = null;
    $HumidInt = null;
    $TempInt = null;
    $TempExt = null;
    $link=null;
?>