<?php
  require_once 'conexao.php';

$opcao = isset($_GET['opcao']) ? $_GET['opcao'] : '';
$valor = isset($_GET['valor']) ? $_GET['valor'] : '';

if (!empty($opcao)) {
    switch ($opcao) {    
    case 'temperatura_interna':
        {
            echo getFilterTemperaturaInterna($valor);
            break;
        }
    case 'temperatura_externa':
        {
            echo getFilterTemperaturaExterna($valor);
            break;
        }
    case 'nivel_gas':
        {
            echo getFilterNivelGas($valor);
            break;
        }        
    }
}

function getFilterTemperaturaInterna($value) {
    $pdo = conecta();
    $sql = 'SELECT * FROM tb_SensorTemperatureInt';
    $stm = $pdo->prepare($sql);
    $stm->bindValue(1, $value);
    $stm->execute();
    sleep(1);
    echo json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
    $pdo = null;
}

function getFilterTemperaturaExterna($value) {
    $pdo = conecta();
    $sql = 'SELECT * FROM tb_SensorTemperatureExt';
    $stm = $pdo->prepare($sql);
    $stm->bindValue(1, $value);
    $stm->execute();
    sleep(1);
    echo json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
    $pdo = null;
}

function getFilterNivelGas($value) {
    $pdo = conecta();
    $sql = 'SELECT * FROM tb_SensorGas';
    $stm = $pdo->prepare($sql);
    $stm->bindValue(1, $value);
    $stm->execute();
    sleep(1);
    echo json_encode($stm->fetchAll(PDO::FETCH_ASSOC));
    $pdo = null;
}

?>