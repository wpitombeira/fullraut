<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$securityKey = $_SERVER['HTTP_SECURITYKEY'];

include('config.php');

$link = DBConnect();

if ($securityKey==md5(SERVER_SEC_KEY)){
    if(file_get_contents('php://input')){
        $json = file_get_contents('php://input');
        $data = json_decode($json);
        
        // GET THE TIME OF THOSE DATAS
        $tmp = $data->metadata->time;
        $date = strstr($tmp, 'T', true);
        $time = substr($tmp, 11, 8);
    
        // RECEIVE DATA FROM PAYLOAD
        $includes = [];
        foreach ($data->payload_fields as $key => $value) {
            array_push($includes, $value);
        }
        
        // insert data into database
        $sql = "INSERT INTO tb_dataSensor VALUES (''";
        foreach ($includes as $key => $value) {
            $sql .= ", '{$value}'";
        }
        $sql .= ", '{$date} {$time}'";
        $sql .= ")";
        if($link->query($sql)){
            http_response_code(200);
        } else {
            http_response_code(500);
        }
    } else {
        http_response_code(500);
    }
} else {
    http_response_code(500);
}

$link = null;
?>
