
    
        google.charts.load('current', {'packages':['gauge']});
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['CO2', 63]
    ]);
    var options = {
        width: 300, height: 320,
        redFrom: 80, redTo: 100,
        yellowFrom:65, yellowTo: 80,
        minorTicks: 5
    };
    var chart = new google.visualization.Gauge(document.getElementById('chart_div'));
    chart.draw(data, options);
    setInterval(function() {
        data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
        chart.draw(data, options);
    }, 3000);
    };

    