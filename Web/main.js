$(document).ready(function(e){
    $('#displayTempInt').html('');
    $.getJSON('webserver/getData.php?opcao=temperatura_interna&valor=1', function (dados){
        if (dados.length > 0){
                var option = '';
                $.each(dados, function(i, obj){
                    option += 'T:'+obj.temperature+' - Hum: '+obj.humidity;
                })
        }else{
            $('#displayTempInt').html('0'); 
        }
        $('#displayTempInt').html(option).show();
    })
})