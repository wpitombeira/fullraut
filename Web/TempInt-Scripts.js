google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Time', 'Temperature', 'Humidity'],
    ['2013',  1000, 400],
    ['2014',  1170, 500],
    ['2015',  660, 600],
    ['2016',  1030, 100]
  ]);

  var options = {
    title: 'Internal Temperature',
    hAxis: {title: 'Time',  titleTextStyle: {color: '#333'}},
    vAxis: {minValue: 0}
  };

  var chart = new google.visualization.AreaChart(document.getElementById('TInt_chart'));
  chart.draw(data, options);
}