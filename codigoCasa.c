#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include "HTTPClient.h"

const char* ssid = "WPSCVIN";
const char* password =  "";
#define ADAF_SERVER "io.adafruit.com"
#define AIO_SERVERPORT 1883
#define ADAF_USERNAME "wpitombeira"
#define AIO_KEY "3a43581a11414a0f8e08df68d248820c"

WiFiClient client;     // Cria uma classe cliente para conexao via MQTT.
Adafruit_MQTT_Client mqtt(&client, ADAF_SERVER, AIO_SERVERPORT, ADAF_USERNAME, AIO_KEY);        // Configura o servico cliente MQTT passando o WiFiClient e os detalhes de MQTT.
Adafruit_MQTT_Subscribe MQTTLBedRoom = Adafruit_MQTT_Subscribe(&mqtt, ADAF_USERNAME "/feeds/LBedRoom");
Adafruit_MQTT_Subscribe MQTTLBathRoom = Adafruit_MQTT_Subscribe(&mqtt, ADAF_USERNAME "/feedsLBathRoom/");
Adafruit_MQTT_Subscribe MQTTLkitch = Adafruit_MQTT_Subscribe(&mqtt, ADAF_USERNAME "/feeds/LKitch");
Adafruit_MQTT_Subscribe MQTTLLivRoom = Adafruit_MQTT_Subscribe(&mqtt, ADAF_USERNAME "/feeds/LLivRoom");
Adafruit_MQTT_Subscribe MQTTLGarage = Adafruit_MQTT_Subscribe(&mqtt, ADAF_USERNAME "/feeds/LGarage");
Adafruit_MQTT_Subscribe MQTTGKey= Adafruit_MQTT_Subscribe(&mqtt, ADAF_USERNAME "/feeds/GKey");

WiFiServer server(80);
String header;
String LBedRoomState = "off";
String LBathRoomState = "off";
String LKitchState = "off";
String LLivRoomState = "off";
String LGarageState = "off";
String GKeyState = "off";
double TempState = 00.00;
double gasState = 00.00;
double HumidState = 00.00;
double lastTempRead = 00.00;
double lastGasRead = 00.00;
double lastHumidRead = 00.00;

String voiceState = "OFF";

// Definicaoo dos Pinos utilizados no projeto

// LEDs de Controle do sistema
const int LYell = 2;
const int LGreen = 4;

// Iluminacao da Residencia
const int LBedRoom = 13;
const int LBathRoom = 12;
const int LKitch = 14;
const int LLivRoom = 27;
const int LGarage = 26;

// Sensores
const int Temp = 22;
const int SensGas = 34;

#define DHTTYPE DHT11

DHT dht(Temp, DHTTYPE);

long previousMillisIHR = 0;
const long IntervalHttpRequest = 15000; // Para uso de temporizador
long previousMillisIGas = 0;
const long IntervalHttpRequestGas = 2000; // Para uso de temporizador

IPAddress local_IP(192, 168, 0, 184); // configura internamente o ESP para ter como padrão o IP 192.168.0.184 (de acordo com configurações de Gateway)
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);
IPAddress secondaryDNS(8, 8, 8, 8);

void MQTT_connect();

void setup() {
    Serial.begin(115200);

    delay(10);

    // Configuracoes de PinMode
    pinMode(LYell, OUTPUT);
    pinMode(LGreen, OUTPUT);
    pinMode(LBedRoom, OUTPUT);
    pinMode(LBathRoom, OUTPUT);
    pinMode(LKitch, OUTPUT);
    pinMode(LLivRoom, OUTPUT);
    pinMode(LGarage, OUTPUT);
    
    // Habilitacao ou Desabilitacao de PinModes
    digitalWrite(LYell,HIGH);
    digitalWrite(LGreen,LOW);
    digitalWrite(LBedRoom,LOW);
    digitalWrite(LBathRoom,LOW);
    digitalWrite(LKitch,LOW);
    digitalWrite(LLivRoom,LOW);
    digitalWrite(LGarage,LOW);

    delay(100);

    // Habilitacao de sensores de Temperatura
    dht.begin();

    if(!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)){
        Serial.println("Falha ao Configurar STA");
    }

    Serial.print("Conectando a rede ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED){
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi Conectado");
    Serial.println("IP -> ");
    Serial.print(WiFi.localIP());
    server.begin();
    mqtt.subscribe(&MQTTLBedRoom);
    mqtt.subscribe(&MQTTLBathRoom);
    mqtt.subscribe(&MQTTLkitch);
    mqtt.subscribe(&MQTTLLivRoom);
    mqtt.subscribe(&MQTTLGarage);
    mqtt.subscribe(&MQTTGKey);
    digitalWrite(LYell,LOW);
    digitalWrite(LGreen,HIGH);
}

uint32_t x=0;

void loop(){
    lastHumidRead = h;
    lastTempRead = t;
    lastGasRead = gasState;
    unsigned long currentMillisIHR = millis();
    if(currentMillisIHR - previousMillisIHR > IntervalHttpRequest){
        previousMillisIHR = currentMillisIHR; // caso TAtual - TAnterior > Intervalo faz com que o TAnterior seja igual o atual;

        float h = dht.readHumidity();
        float t = dht.readTemperature();

        TempState = t;
        HumidState = h;
        Serial.println("Temp");
        Serial.print(t);
    }
    unsigned long currentMillisGas = millis();
    if(currentMillisGas - previousMillisIGas > IntervalHttpRequestGas){
      previousMillisIGas = currentMillisGas; // caso TAtual - TAnterior > Intervalo faz com que o TAnterior seja igual o atual;

      float gasLevel = analogRead(SensGas);
        gasState = gasLevel;
        Serial.print("Gas");
        Serial.println(gasLevel);
    }

    //  TRECHO COMENTADO PARA FUTURA IMPLEMENTAÇÃO COM WEBSERVER
    //  INTEGRAÇÃO 66% REALIZADA
    //   HTTPClient http;
    //   http.begin("http://raut.tamec.com.br/WebServer/sendData.php");
    //   http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    //   int httpResponseCode = http.POST("TempInt=" + String(tInt) + "&HumidInt=" + String(hInt) + "&TempExt=" + String(tExt) + "&HumidExt=" + String(hExt) + "&gas=" + String(gasLevel));

    //   if (httpResponseCode > 0){
    //     String response = http.getString();
    //     Serial.println(httpResponseCode);
    //     Serial.println(response);
    //   } else {
    //     Serial.print("Error Sending Data");
    //     Serial.println(httpResponseCode);
    //   }
    //   http.end();      
    // }
    if(voiceState=="ON"){
        MQTT_connect(); // Cria uma conexao MQTT com o servidor Adafruit
        Adafruit_MQTT_Subscribe *subscription; // Aguarda o servidor enviar uma solicitacao para o ESP.
        while ((subscription = mqtt.readSubscription(5000))) {
            if (subscription == &MQTTLBedRoom) {
            Serial.print(F("Got: "));
            Serial.println((char *)MQTTLBedRoom.lastread);
            if (!strcmp((char*) MQTTLBedRoom.lastread, "ON")){
                digitalWrite(LBedRoom, HIGH);
                LBedRoomState = "on";
            } else{
                digitalWrite(LBedRoom, LOW);
                LBedRoomState = "off";
            }
            }
            if (subscription == &MQTTLkitch) {
                Serial.print(F("Got: "));
                Serial.println((char *)MQTTLkitch.lastread);
                if (!strcmp((char*) MQTTLkitch.lastread, "ON")){
                digitalWrite(LKitch, HIGH);
                LKitchState = "on";
                } else{
                digitalWrite(LKitch, LOW);
                LKitchState = "off";
                }
            }
            if (subscription == &MQTTLGarage) {
                Serial.print(F("Got: "));
                Serial.println((char *)MQTTLGarage.lastread);
                if (!strcmp((char*) MQTTLGarage.lastread, "ON")){
                digitalWrite(LGarage, HIGH);
                LGarageState = "on";
                } else {
                digitalWrite(LGarage, LOW);
                LGarageState = "off";
                }
            }
            if (subscription == &MQTTLLivRoom) {
                Serial.print(F("Got: "));
                Serial.println((char *)MQTTLLivRoom.lastread);
                if (!strcmp((char*) MQTTLLivRoom.lastread, "ON")){
                digitalWrite(LLivRoom, HIGH);
                LLivRoomState = "on";
                } else{
                digitalWrite(LLivRoom, LOW);
                LLivRoomState = "off";
                }
            }
            if (subscription == &MQTTLBathRoom) {
                Serial.print(F("Got: "));
                Serial.println((char *)MQTTLBathRoom.lastread);
                if (!strcmp((char*) MQTTLBathRoom.lastread, "ON")){
                digitalWrite(LBathRoom, HIGH);
                LBathRoomState = "on";
                }
                else{
                digitalWrite(LBathRoom, LOW);
                LBathRoomState = "off";
                }
            }
            if (subscription == &MQTTGKey) {
                Serial.print(F("Got: "));
                Serial.println((char *)MQTTGKey.lastread);
                if (!strcmp((char*) MQTTGKey.lastread, "ON")){
                LGarageState = "on";
                LLivRoomState = "on";
                LKitchState = "on";
                LBathRoomState = "on";
                LBedRoomState = "on";
                GKeyState = "on";
                digitalWrite(LBedRoom,HIGH);
                digitalWrite(LBathRoom,HIGH);
                digitalWrite(LKitch,HIGH);
                digitalWrite(LLivRoom,HIGH);
                digitalWrite(LGarage,HIGH);
                } else {
                LGarageState = "off";
                LLivRoomState = "off";
                LKitchState = "off";
                LBathRoomState = "off";
                LBedRoomState = "off";
                GKeyState = "off";
                digitalWrite(LBedRoom,LOW);
                digitalWrite(LBathRoom,LOW);
                digitalWrite(LKitch,LOW);
                digitalWrite(LLivRoom,LOW);
                digitalWrite(LGarage,LOW);
                }
            }
        }
    }
    WiFiClient client = server.available();   // Aguarda por conexoes no webserver

    if (client) {                               // Recebe nova conexao de usuario
        Serial.println("Novo Cliente.");          // Avisa no SerialMonitor que um novo usuario conectou
        String currentLine = "";                // Cria uma string para enviar os dados do usuario conectado
        while (client.connected()) {            // Gera um loop enquanto o usuario estiver conectado
            if (client.available()) {             // Se existe alguma informacao a ser lida pelo cliente
                char c = client.read();             // le um byte, entao
                Serial.write(c);                    // imprime ele no SerialMonitor
                header += c;
                if (c == '\n') {
                    if (currentLine.length() == 0) {
                        // O HTTP Header sempre inicia com uma resposta, no caso 200 OK
                        client.println("HTTP/1.1 200 OK");
                        client.println("Content-type:text/html");
                        client.println("Connection: close");
                        client.println();

                        // Configuracoes de GPIO
                        if (header.indexOf("GET /13/on") >= 0) {
                            Serial.println("LBedRoom Light on");
                            LBedRoomState = "on";
                            digitalWrite(LBedRoom, HIGH);
                        } else if (header.indexOf("GET /13/off") >= 0) {
                            Serial.println("LBedRoom Light off");
                            LBedRoomState = "off";
                            digitalWrite(LBedRoom, LOW);
                        } else if (header.indexOf("GET /voice") >= 0) {
                          if(voiceState=="OFF"){
                            voiceState="ON";
                          } else {
                            voiceState="OFF";
                          }
                        } else if (header.indexOf("GET /12/on") >= 0) {
                            Serial.println("LBathRoom Light on");
                            LBathRoomState = "on";
                            digitalWrite(LBathRoom, HIGH);
                        } else if (header.indexOf("GET /12/off") >= 0) {
                            Serial.println("LBathRoom Light off");
                            LBathRoomState = "off";
                            digitalWrite(LBathRoom, LOW);
                        } else if (header.indexOf("GET /14/on") >= 0) {
                            Serial.println("LKitch Light on");
                            LKitchState = "on";
                            digitalWrite(LKitch, HIGH);
                        } else if (header.indexOf("GET /14/off") >= 0) {
                            Serial.println("LKitch Light off");
                            LKitchState = "off";
                            digitalWrite(LKitch, LOW);
                        } else if (header.indexOf("GET /27/on") >= 0) {
                            Serial.println("LlivRoom Light on");
                            LLivRoomState = "on";
                            digitalWrite(LLivRoom, HIGH);
                        } else if (header.indexOf("GET /27/off") >= 0) {
                            Serial.println("LlivRoom Light off");
                            LLivRoomState = "off";
                            digitalWrite(LLivRoom, LOW);
                        } else if (header.indexOf("GET /26/on") >= 0) {
                            Serial.println("LGarage Light on");
                            LGarageState = "on";
                            digitalWrite(LGarage, HIGH);
                        } else if (header.indexOf("GET /26/off") >= 0) {
                            Serial.println("LGarage Light off");
                            LGarageState = "off";
                            digitalWrite(LGarage, LOW);
                        }  else if (header.indexOf("GET /G/on") >= 0) {
                            Serial.println("GKey Light on");
                            LGarageState = "on";
                            LLivRoomState = "on";
                            LKitchState = "on";
                            LBathRoomState = "on";
                            LBedRoomState = "on";
                            GKeyState = "on";
                            digitalWrite(LGarage, HIGH);
                            digitalWrite(LLivRoom, HIGH);
                            digitalWrite(LKitch, HIGH);
                            digitalWrite(LBathRoom, HIGH);
                            digitalWrite(LBedRoom, HIGH);
                        } else if (header.indexOf("GET /G/off") >= 0) {
                            Serial.println("GKey Light off");
                            LGarageState = "off";
                            LLivRoomState = "off";
                            LKitchState = "off";
                            LBathRoomState = "off";
                            LBedRoomState = "off";
                            GKeyState = "off";
                            digitalWrite(LGarage, LOW);
                            digitalWrite(LLivRoom, LOW);
                            digitalWrite(LKitch, LOW);
                            digitalWrite(LBathRoom, LOW);
                            digitalWrite(LBedRoom, LOW);
                        }

                        // Exibicao da pagina WEB
                        client.println("<!DOCTYPE html><html lang=\"pt-br\">");
                        client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
                        client.println("<link rel=\"icon\" href=\"data:,\">");
                        client.println("<link rel=\"stylesheet\" href=\"http://raut.tamec.com.br/Raut61-A-Styles.css\">");
                        client.println("<link rel=\"stylesheet\" href=\"http://tamec.com.br/site/lib/bootstrap/css/bootstrap.min.css\">");
                        client.println("<link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700|Philosopher:400,700\">");

                        client.println("<body><h1 class=\"text-center\">Casa Autonoma Raut61-A</h1>");

                        client.println("<a href=\"/voice\"><button class=\"btn btn-danger btn-mqtt\">Voice<br>"+ voiceState +"</button></a>");

                        client.println("<div class=\"box-area col-12\"><div class=\"box col-md-3 col-5\">");
                        client.println("<p>Luzes Quarto</p>  <span>" + LBedRoomState + "</span>");
                        if (LBedRoomState=="off") {
                            client.println("<p><a href=\"/13/on\"><button class=\"btn-lg btn-primary\">ON</button></a></p>");
                        } else {
                            client.println("<p><a href=\"/13/off\"><button class=\"btn-lg btn-secondary\">OFF</button></a></p>");
                        }
                        client.println("</div>");

                        client.println("<div class=\"box col-md-3 col-5\">");
                        client.println("<p>Luzes Banheiro</p>  <span>" + LBathRoomState + "</span>");
                        if (LBathRoomState=="off") {
                            client.println("<p><a href=\"/12/on\"><button class=\"btn-lg btn-primary\">ON</button></a></p>");
                        } else {
                            client.println("<p><a href=\"/12/off\"><button class=\"btn-lg btn-secondary\">OFF</button></a></p>");
                        }
                        client.println("</div>");

                        client.println("<div class=\"box col-md-3 col-5\">");
                        client.println("<p>Luzes Cozinha</p>  <span>" + LKitchState + "</span>");
                        if (LKitchState=="off") {
                            client.println("<p><a href=\"/14/on\"><button class=\"btn-lg btn-primary\">ON</button></a></p>");
                        } else {
                            client.println("<p><a href=\"/14/off\"><button class=\"btn-lg btn-secondary\">OFF</button></a></p>");
                        }
                        client.println("</div>");

                        client.println("<div class=\"box col-md-3 col-5\">");
                        client.println("<p>Luzes Sala</p>  <span>" + LLivRoomState + "</span>");
                        if (LLivRoomState=="off") {
                            client.println("<p><a href=\"/27/on\"><button class=\"btn-lg btn-primary\">ON</button></a></p>");
                        } else {
                            client.println("<p><a href=\"/27/off\"><button class=\"btn-lg btn-secondary\">OFF</button></a></p>");
                        }
                        client.println("</div>");

                        client.println("<div class=\"box col-md-3 col-5\">");
                        client.println("<p>Luzes Garagem</p>  <span>" + LGarageState + "</span>");
                        if (LGarageState=="off") {
                            client.println("<p><a href=\"/26/on\"><button class=\"btn-lg btn-primary\">ON</button></a></p>");
                        } else {
                            client.println("<p><a href=\"/26/off\"><button class=\"btn-lg btn-secondary\">OFF</button></a></p>");
                        }
                        client.println("</div>");
                        
                        client.println("<div class=\"box col-md-3 col-5 chave-geral\">");
                        client.println("<p>Chave Geral</p>  <span>" + GKeyState + "</span>");
                        if (GKeyState=="off") {
                            client.println("<p><a href=\"/G/on\"><button class=\"btn-lg btn-primary\">ON</button></a></p>");
                        } else {
                            client.println("<p><a href=\"/G/off\"><button class=\"btn-lg btn-secondary\">OFF</button></a></p>");
                        }
                        client.println("</div>");
                        client.println("</div>");

                        String gasSituation = "OK";
                        if(gasState>1500) gasSituation="PERIGO";
                        client.println("<div class=\"box-areas col-12\"><div class=\"box col-md-3\">Nível de Gás "+String(gasState)+" - "+gasSituation+"<br>Ultima Leitura: "+String(lastGasRead)+"</div>");
                        client.println("<div class=\"box col-md-3\">Humidade "+String(HumidState)+"%<br>Ultima Leitura: "+String(lastHumidRead)+"</div>");
                        client.println("<div class=\"box col-md-3\">Temperatura "+String(TempState)+" ºC<br>Ultima Leitura: "+String(lastTempRead)+"</div></div>");

                        client.println("<script src=\"http://tamec.com.br/site/lib/bootstrap/js/bootstrap.min.js\"></script>");
                        client.println("<script src=\"http://tamec.com.br/site/lib/jquery/jquery.min.js\"></script>");
                        client.println("<script src=\"http://raut.tamec.com.br/main.js\"></script>");
                        client.println("<script>$(document).ready(function($){ $(\"span\").each(function (i) { if($this).text() == \"off\") { $(this).addClass(\"OffClass\"); } else { $(this).addClass(\"OnClass\"); } }); });</script>");
                        client.println("</body></html>");
                        client.println();
                        break;
                    } else {
                        currentLine = "";
                    }
                } else if (c != '\r') {
                    currentLine += c;
                }
            }
        }
        header = "";
        client.stop();

        Serial.println("Client desconectado.");
        Serial.println("");
    }
}

void MQTT_connect() {
    int8_t ret;
    if (mqtt.connected()) {
        return;
    }
    Serial.print("Connecting to MQTT... ");
    uint8_t retries = 3;
    while ((ret = mqtt.connect()) != 0) { // Caso conectado ira retornar 0
        Serial.println(mqtt.connectErrorString(ret));
        Serial.println("Tentando Conectar via MQTT Novamente em 5 segundos...");
        mqtt.disconnect();
        delay(5000);
        retries--;
        if (retries == 0) {
            // Basicamente Desconecta e aguarda nova requisicao MTE.
            while (1);
        }
    }
    Serial.println("MQTT Conectado!");
}
